\contentsline {section}{\numberline {1}Introduction}{2}{section.1}%
\contentsline {subsection}{\numberline {1.1}Problem Restatement}{2}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Problem Analysis and Our Work}{2}{subsection.1.2}%
\contentsline {section}{\numberline {2}Assumptions and Justifications}{2}{section.2}%
\contentsline {section}{\numberline {3}Notations}{3}{section.3}%
\contentsline {section}{\numberline {4}Establishing the model}{3}{section.4}%
\contentsline {subsection}{\numberline {4.1}Establishment of Star Rating and Review Relationships}{3}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}The Process of Reviews Emotion Analysis}{3}{subsubsection.4.1.1}%
\contentsline {subsubsection}{\numberline {4.1.2}Experimental Results and Data Preprocessing}{4}{subsubsection.4.1.2}%
\contentsline {subsection}{\numberline {4.2}Construction of Comment Measures: GHEP Model}{4}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Determination of Score $\mathbf {G}$ based on NLP}{5}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Setting of other Indicators}{6}{subsubsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.3}Comment Quantitative Value $\mathbf {C}^{*}$}{7}{subsubsection.4.2.3}%
\contentsline {subsection}{\numberline {4.3}Rating-Review-based Evaluation Metrics}{7}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Various Product Metrics}{8}{subsection.4.4}%
\contentsline {section}{\numberline {5}Solving the Model}{9}{section.5}%
\contentsline {subsection}{\numberline {5.1}Time Effects of Product Measures}{9}{subsection.5.1}%
\contentsline {subsubsection}{\numberline {5.1.1}Hairdryer}{9}{subsubsection.5.1.1}%
\contentsline {subsubsection}{\numberline {5.1.2}Microwave}{9}{subsubsection.5.1.2}%
\contentsline {subsection}{\numberline {5.2}The Outlook of Product}{9}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Causal Inference of Specific Star Ratings and Reviews}{11}{subsection.5.3}%
\contentsline {subsubsection}{\numberline {5.3.1}A Brief Introduction to Causal Inference}{11}{subsubsection.5.3.1}%
\contentsline {subsubsection}{\numberline {5.3.2}Propensity Score Matching based on Bootstrap}{11}{subsubsection.5.3.2}%
\contentsline {subsubsection}{\numberline {5.3.3}Process and via PSM based on bootstrap to data}{12}{subsubsection.5.3.3}%
\contentsline {subsection}{\numberline {5.4}Relationship between Rating and Specific Quality Descriptors}{15}{subsection.5.4}%
\contentsline {section}{\numberline {6}Sensitivity Analysis}{16}{section.6}%
\contentsline {section}{\numberline {7}Strengths and Weaknesses}{17}{section.7}%
\contentsline {subsection}{\numberline {7.1}Strengths}{17}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Weaknesses}{18}{subsection.7.2}%
\contentsline {subsection}{\numberline {7.3}Further Discussion}{18}{subsection.7.3}%
\contentsline {section}{\numberline {8}Strategy Recommendation and Conclusions}{19}{section.8}%
\contentsline {section}{Appendices}{21}{section*.13}%
\contentsline {section}{Appendix \numberline {A}Code for Part 3.1}{21}{appendix.a.A}%
\contentsline {section}{Appendix \numberline {B}Code for Part 3.3}{25}{appendix.a.B}%
